# Distributed Computing "Cloud-Datenmanagement"

## Aufgabenstellung
Die detaillierte [Aufgabenstellung](TASK.md) beschreibt die notwendigen Schritte zur Realisierung.

## Design und Beschreibung

Diese Applikation zur Benutzeranmeldung basiert auf dem Framework Spring und benutzt zur Umsetzung  das Spring Security Modul. 

Die Daten der Benutzer werden in einer H2 Datenbank gespeichert, welche Datei basiert ist. Um diese benutzen zu können, muss die H2 Datenbank in den `application.properties` der Spring Applikation konfiguriert werden. Dabei wird der Speicherort und Modus (create | update) festgelegt.

```properties
spring.datasource.url=jdbc:h2:file:./data/user_db
spring.datasource.username=admin
spring.datasource.password=userdata
spring.datasource.driver-class-name=org.h2.Driver
# Use "create" or "create-drop" when you wish to recreate database on restart;
# use "update" or "validate" when data is to be kept.
spring.jpa.hibernate.ddl-auto=update
```

Die Interaktion mit der Datenbank im Programm selbst wird mithilfe von JPA realisiert, wofür aber Spring ein `JpaRepository` zur Verfügung stellt. Mit diesen kann man eigene Interfaces erstellen, in denen man CRUD-Operationen mittels Methodennamen (z.B. `findByEmailAndPassword`) schreiben kann, die zur Laufzeit dynamisch erstellt werden. Zum Interagieren mit der Benutzerdatenbank wird in der Applikaton das `UserRepository` verwenden, was mit den Benutzerdaten (Klasse `User`) arbeitet.

Das Registrieren von Benutzern ist als Post-Schnittstelle unter `/registration` implementiert. Das Mapping dafür befindet sich in der Klasse `WebController`. Die Klasse enhält auch eine Get-Schnittstelle für `/registration`, welche zum Zurückgeben einer Website zur Registration dient. Diese Website wird dynamisch mittels der Thymeleaf Template Engine erstellt und zeigt ein Formular an, in der sich  ein Benutzer registrieren kann. Beim Abschicken des Formulars wird dann die Post-Schnittstelle für `/registration` aufgerufen. Alternativ kann man auch einfach `curl` verwenden, wobei die Benutzerdaten als URL-Parameter angehängt werden (macht das Formular im Hintergrund auch). 

Die Post-Schnittstelle für `registration` macht nichts anderes als zu schauen, ob alle Parameter korrekt ausgefüllt sind und ob ein Benutzer mit der gleichen Email-Adresse nicht schon vorhanden ist. Wenn alle Kriterien erfüllt sind, wird der Benutzer mittels  des `UserRepository` abgespeichert.

```kotlin
@Autowired
lateinit var repository: UserRepository

@PostMapping(value = ["/registration"])
fun registrationProcess(@ModelAttribute("user") newUser: @Valid User?,
        result: BindingResult, request: WebRequest?, errors: Errors?): ModelAndView? {

    val user = newUser ?: User()

    val match = repository.findByEmail(user.email)

    if (match != null) {
        result.rejectValue("email", "email.error", "Email is already in use")
        return ModelAndView("registration", "user", user)
    }

    var correctInput = true
    if (user.email == "") {
        result.rejectValue("email", "email.error", "Validation Error")
        correctInput = false
    }
    if (user.firstName == "") {
        result.rejectValue("firstName", "firstname.error", "Validation Error")
        correctInput = false
    }
    if (user.lastName == "") {
        result.rejectValue("lastName", "lastname.error", "Validation Error")
        correctInput = false
    }
    if (user.password == "") {
        result.rejectValue("password", "password.error", "Validation Error")
        correctInput = false
    }

    return if (correctInput) {
        repository.save(user)
        ModelAndView("redirect:/hello")
    } else {
        ModelAndView("registration", "user", user)
    }
}
```

Das Login funktioniert auf den selben Prinzip wie die Registrierung. Doch im Gegensatz zu dieserm, muss das Login nicht implementiert werden, da Spring Security standardmäßig dieses unter `/login` zur Verfügung stellt. Ein Get-Request auf diese Adresse gibt ein Login-Formular zurück, eine Post-Request dient für den Login-Prozess. 

Das einzige was ergänzt werden muss, ist ein individueller Anmeldeprozess. Standardmäßig überrpüft Spring-Security die Anmeldedaten in einer speicher-eigenen Konfiguration. Um aber die H2 Datenbank zur Authentifizierung zu verwenden, überschreibt man einfach die Spring interne Methode zur Authentifikation. 

```kotlin
class CustomAuthenticationProvider : AuthenticationProvider {
    @Autowired
    lateinit var repository: UserRepository

    @Throws(AuthenticationException::class)
    override fun authenticate(authentication: Authentication): Authentication? {
        val email: String = authentication.name
        val password: String = authentication.credentials.toString()

        repository.findByEmailAndPassword(email, password) ?: throw BadCredentialsException("Invalid Credentials")

        return UsernamePasswordAuthenticationToken(email, password, ArrayList())

    }
    ...
}
```

Am Anfang werden aus den empfangenen Authentifikations Objekt die Anmeldedaten herausgesucht, diese werden dann in einer Datenbankabfrage verwendet. Gibt die Abrage `null` zurück, bedeutet dies, dass der User nicht vorhanden ist, oder ein falsches Passwort eingegeben hat. Dann wird eine Exception geworfen, die auf dem Login-Formular grafisch den Fehler zurück gibt. Wird ein Benutzer gefunden, bedeutet dies, dass die Anmeldedaten korrekt sind. Dann wird dieser Benutzer zurückgegeben und von Spring Security verwaltet. Dies bedeutet, dass die Applikaten den User bei neuen Anfragen erkennen kann (durch Session/Token) und geschützte Schnittstellen (z.B. `/hello`) freigibt.

In der Praxis sieht das Konzept dann so aus:

Unter `/registration`meldet sich ein neuer Nutzer an.

![](images/registrationV.PNG)

Beim Absenden des Formulares wird die Post-Schnittstelle von `/registration` abgerufen, die den Registrierungsprozess der Applikation startet.

![](images/registration.PNG)

Bei erfolgreicher Registrierung wird auf `/hello` umgeleitet, da aber dieser Bereich geschützt wird, wird man vorher auf die Login-Seite umgeleitet. Auf dieser kann man sich mit dem gerade registrierten Account anmelden. 

![](images/loginV.PNG)

Beim Absenden des Formulares wird die Post-Schnittstelle von `/login`abgerufen, die den Login-Prozess startet.

![](images/login.PNG)

Bei erfolgreicher Anmeldung wird man auf den zuvor gesperten Bereich `/hello` weitergeleitet, wo eine personalisierte Nachricht erscheint. 

![](images/hello.PNG)



## Deployment

### Projekt herunterladen

```
git clone https://github.com/TGM-HIT/syt5-gk933-cloud-datamanagement-kurbaniec-tgm.git
cd syt5-gk933-cloud-datamanagement-kurbaniec-tgm
```

### Tomcat Server der Applikation konfigurieren

Dazu die `application.properties` unter `src/main/resources` bearbeiten

* `server.address` - IP-Adresse des Hosts
* `server.port` - Port der für die Applikation genutzt werden soll

Standardmäßig ist `localhost:8000` eingestellt

### Applikation starten

```
gradlew bootRun
```

### Applikation testen

```
gradlew test
```



## Quellen

* [Spring Security | 03.12.2019](https://spring.io/guides/gs/securing-web/)
* [Spring + H2 Database | 03.12.2019](https://attacomsian.com/blog/spring-data-jpa-h2-database)
* [Spring + H2 Database | 03.12.2019](https://www.baeldung.com/spring-boot-h2-database)
* [Spring + H2 Database | 03.12.2019](https://bitbucket.org/kurbaniec/sew4-userdata-kurbaniec-tgm/src/master/)
* [H2 Database | 03.12.2019](http://www.h2database.com/html/features.html?highlight=init&search=init#firstFound)
* [Spring Custom Authentication | 03.12.2019](https://www.baeldung.com/spring-security-authentication-provider)
* [Spring Authenticatior triggers twice | 03.12.2019](https://stackoverflow.com/questions/33788120/providermanager-authenticate-called-twice-for-badcredentialsexception)
* [Spring Testing custom properites | 03.12.2019](https://stackoverflow.com/questions/29669393/override-default-spring-boot-application-properties-settings-in-junit-test)
* [Spring Testing MockMvc | 06.12.2019](https://github.com/Orange-OpenSource/spring-security-formlogin-restbasic/blob/master/src/test/java/com/orange/spring/demo/biz/security/LoginTest.java)
* [Spring Testing MockMvc User Details | 06.12.2019](https://stackoverflow.com/questions/15203485/spring-test-security-how-to-mock-authentication)
* [Spring Testing MockMvc MockSession | 06.12.2019](https://github.com/spring-projects/spring-framework/issues/18393)
* [Spring Password Encoding | 14.12.2019](https://www.baeldung.com/spring-security-registration-password-encoding-bcrypt)
* [Spring BCryptPasswordEncoder Docs | 14.12.2019](https://docs.spring.io/spring-security/site/docs/4.2.12.RELEASE/apidocs/org/springframework/security/crypto/bcrypt/BCryptPasswordEncoder.html)
* [Random Long String | 14.12.2019](https://www.baeldung.com/java-random-string)
