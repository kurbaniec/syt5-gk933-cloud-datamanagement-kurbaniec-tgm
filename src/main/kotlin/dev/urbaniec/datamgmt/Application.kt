package dev.urbaniec.datamgmt

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

import org.springframework.security.crypto.password.PasswordEncoder


@SpringBootApplication
class Application {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            runApplication<Application>(*args)
        }

        /**
         * Uncomment this, if you want to generate users on startup.
         * If you this, change the H2 mode from `update` to `create`-
         */

        /**
        @Bean
        fun loadData(repository: UserRepository): CommandLineRunner? {
            return CommandLineRunner { args: Array<String?>? ->
                // clear all data before start
                repository.deleteAll()
                // save a couple of users
                repository.save(User(0,"test@test", "Mustermann", "Max", "Baum"))
                repository.save(User(0,"da@test", "Musterfrau", "Maxine","Da"))
                // fetch all users
                println("Users found with findAll():")
                println("-------------------------------")
                for (customer in repository.findAll()) {
                    println(customer.toString())
                }
                println("")
                // fetch user by email
                println("Customer found with findByLastNameStartsWithIgnoreCase('Bauer'):")
                println("--------------------------------------------")
                for (bauer in repository
                        .findByEmailStartsWithIgnoreCase("test@test")) {
                    println(bauer.toString())
                }
                println("")
            }
        }
        */
    }
}


