package dev.urbaniec.datamgmt.controller

import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry

import org.springframework.web.servlet.config.annotation.WebMvcConfigurer




/**
 * Configuration for Spring MVC (Servlet Stack).
 *
 * @author Kacper Urbaniec
 * @version 2019-12-06
 */
@Configuration
class MvcConfig : WebMvcConfigurer {
    override fun addViewControllers(registry: ViewControllerRegistry) {
        registry.addViewController("/home").setViewName("index")
        registry.addViewController("/").setViewName("index")
        registry.addViewController("/hello").setViewName("hello")
        registry.addViewController("/login").setViewName("login")
    }
}