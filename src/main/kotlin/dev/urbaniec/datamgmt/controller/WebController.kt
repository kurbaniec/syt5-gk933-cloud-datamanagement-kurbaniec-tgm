package dev.urbaniec.datamgmt.controller

import dev.urbaniec.datamgmt.data.User
import dev.urbaniec.datamgmt.data.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.validation.Errors
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.ModelAndView
import javax.validation.Valid


/**
 * Controller that adds Rest-Endpoints to the application.
 *
 * In this case, the endpoints are used to register new user.
 *
 * @author Kacper Urbaniec
 * @version 2019-12-03
 */
@Controller
class WebController {


    @Autowired
    lateinit var repository: UserRepository

    @Autowired
    lateinit var passwordEncoder: PasswordEncoder

    @GetMapping(value = ["/registration"])
    fun registration(request: WebRequest, model: Model): String {
        val newUser = User()
        model.addAttribute("user", newUser)
        return "registration"
    }

    @PostMapping(value = ["/registration"])
    fun registrationProcess(@ModelAttribute("user") newUser: @Valid User?,
            result: BindingResult, request: WebRequest?, errors: Errors?): ModelAndView? {

        val user = newUser ?: User()

        val match = repository.findByEmail(user.email)

        if (match != null) {
            result.rejectValue("email", "email.error", "Email is already in use")
            return ModelAndView("registration", "user", user)
        }

        var correctInput = true
        if (user.email == "") {
            result.rejectValue("email", "email.error", "Validation Error")
            correctInput = false
        }
        if (user.firstName == "") {
            result.rejectValue("firstName", "firstname.error", "Validation Error")
            correctInput = false
        }
        if (user.lastName == "") {
            result.rejectValue("lastName", "lastname.error", "Validation Error")
            correctInput = false
        }
        if (user.password == "") {
            result.rejectValue("password", "password.error", "Validation Error")
            correctInput = false
        }

        return if (correctInput) {
            user.password = passwordEncoder.encode(user.password)
            repository.save(user)
            ModelAndView("redirect:/hello")
        } else {
            ModelAndView("registration", "user", user)
        }
    }
}