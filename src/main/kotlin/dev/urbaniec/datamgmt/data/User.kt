package dev.urbaniec.datamgmt.data

import javax.persistence.*
import javax.validation.constraints.Email
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull


/**
 * Data structure for a User.
 *
 * @author Kacper Urbaniec
 * @version 2019-12-03
 */
@Entity
data class User(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0,
    @NotNull
    @NotEmpty
    @Email
    var email: String,
    @NotNull
    @NotEmpty
    var firstName: String,
    @NotNull
    @NotEmpty
    var lastName: String,
    @NotNull
    @NotEmpty
    var password: String
) {
    constructor(email: String, firstName: String, lastName: String, password: String): this(0, email, firstName, lastName, password)
    constructor(): this(0, "", "","", "")
}