package dev.urbaniec.datamgmt.data

import org.springframework.data.jpa.repository.JpaRepository

/**
 * Module that allows interacting with the H2 database.
 *
 * @author Kacper Urbaniec
 * @version 2019-12-03
 */
public interface UserRepository: JpaRepository<User, Long> {
    fun findByEmailStartsWithIgnoreCase(email: String): List<User>

    fun findByEmailAndPassword(email: String, password: String): User?

    fun findByEmailAndFirstNameAndLastName(
            email: String, firstname: String, lastName: String): User?

    fun findByEmailAndFirstNameAndLastNameAndPassword(
            email: String, firstname: String, lastName: String, password: String): User?

    fun findByEmail(email: String): User?
}