package dev.urbaniec.datamgmt.security

import dev.urbaniec.datamgmt.data.User
import dev.urbaniec.datamgmt.data.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component
import java.util.*


/**
 * Configuration for Spring Security.
 * Used to set up secured and non-secured endpoints.
 *
 * @author Kacper Urbaniec
 * @version 2019-12-03
 */
@Configuration
@EnableWebSecurity
class WebSecurityConfig : WebSecurityConfigurerAdapter() {

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/", "home", "/registration").permitAll()
                .antMatchers(HttpMethod.POST, "/registration").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .defaultSuccessUrl("/hello")
                .permitAll()
                .and()
                .logout()
                .permitAll()
                .and()
                .csrf().disable()
    }

    @Bean
    fun passwordEncoder(): PasswordEncoder? {
        return BCryptPasswordEncoder()
    }
}

/**
 * Provides custom authentication, that uses the H2 database to check user credentials.
 *
 * @author Kacper Urbaniec
 * @version 2019-12-03
 */
@Component
class CustomAuthenticationProvider : AuthenticationProvider {
    @Autowired
    lateinit var repository: UserRepository

    @Autowired
    lateinit var passwordEncoder: PasswordEncoder

    @Throws(AuthenticationException::class)
    override fun authenticate(authentication: Authentication): Authentication? {
        val email: String = authentication.name
        val password: String = authentication.credentials.toString()
        //repository.findByEmailAndPassword(email, password) ?: throw BadCredentialsException("Invalid Credentials")
        val check = repository.findByEmail(email) ?: throw UsernameNotFoundException("Username not found")
        if (email == check.email && passwordEncoder.matches(password, check.password)) {
            return UsernamePasswordAuthenticationToken(email, password, ArrayList())
        }
        throw BadCredentialsException("Invalid Credentials")

    }

    override fun supports(authentication: Class<*>): Boolean {
        return authentication == UsernamePasswordAuthenticationToken::class.java
    }
}