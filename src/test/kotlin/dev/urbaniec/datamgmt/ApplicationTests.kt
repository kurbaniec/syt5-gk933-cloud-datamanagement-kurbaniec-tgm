package dev.urbaniec.datamgmt

import dev.urbaniec.datamgmt.data.User
import dev.urbaniec.datamgmt.data.UserRepository
import org.apache.commons.lang3.RandomStringUtils
import org.hamcrest.CoreMatchers.containsString
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.mock.web.MockHttpSession
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.logout
import org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated
import org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity
import org.springframework.test.context.TestPropertySource
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import org.springframework.web.util.NestedServletException
import java.net.URLDecoder


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations=["classpath:test.properties"])
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ApplicationTests {

    @Autowired
    lateinit var repository: UserRepository

    @Autowired
    lateinit var  context: WebApplicationContext

    @Autowired
    lateinit var passwordEncoder: PasswordEncoder

    lateinit var mockMvc: MockMvc

    @BeforeAll
    fun setup() {
        // Setup Spring Security for MockMvc
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply<DefaultMockMvcBuilder>(springSecurity())
                .build()
        // Add valid user
        repository.save(User(0,"valid@user", "Valid", "User", passwordEncoder.encode("test")))
    }

    @Test
    fun loginPageIsAvailableWhenNotAuthenticated() {
        mockMvc
                .perform(get("/login"))
                .andExpect(status().isOk)
    }

    @Test
    @WithMockUser(username = "mock@user", password = "test", roles = ["USER"])
    fun securedPageIsAvailableWhenAuthenticatedWithMockedUser() {
        mockMvc
                .perform(get("/hello"))
                .andExpect(status().isOk)
                .andExpect(content().string(containsString("Hello mock@user!")))
    }

    @Test
    fun securedPageIsAvailableWhenAuthenticated() {
        val session = mockMvc
                .perform(formLogin().user("valid@user").password("test"))
                .andExpect(authenticated().withUsername("valid@user"))
                .andReturn().request.session
        mockMvc
                .perform(get("/hello").session(session as MockHttpSession))
                .andExpect(status().isOk)
                .andExpect(content().string(containsString("Hello valid@user!")))
    }

    @Test
    fun securedPageIsNotAvailableWhenNotAuthenticated () {
        mockMvc
                .perform(get("/hello"))
                .andExpect(status().isFound)
    }

    @Test
    fun validUserCanLoginAndLogout() {
        mockMvc
                .perform(formLogin().user("valid@user").password("test"))
                .andExpect(status().isFound)
                .andExpect(redirectedUrl("/hello"))
                .andExpect(authenticated().withUsername("valid@user"))
        mockMvc
                .perform(logout())
                .andExpect(status().isFound)
                .andExpect(redirectedUrl("/login?logout"))
    }

    @Test
    fun invalidUserCanNotLogin() {
        val errorUrl = "/login?error"
        mockMvc
                .perform(formLogin().user("invalid").password("invalid"))
                .andExpect(status().isFound)
                .andExpect(redirectedUrl(errorUrl))
                .andExpect(unauthenticated())
        mockMvc
                .perform(get(errorUrl))
                .andExpect(content().string(containsString("Invalid credentials")))
    }

    @Test
    fun successfulRegistration() {
        // Define new user
        val user = mapOf(
                "email" to "newuser@test", "firstName" to "New", "lastName" to "User", "password" to "password")
        // Create post body for query
        var query = ""
        for((k, v) in user) {
            query += "$k=$v&"
        }
        query = query.dropLast(1)
        mockMvc
                .perform(post("/registration?$query"))
                .andExpect(status().isFound)
                .andExpect(redirectedUrl("/hello"))
        // Check if user got successfully registered
        val check = repository.findByEmailAndFirstNameAndLastName("newuser@test", "New", "User")
        assertNotNull(check)
        assert(passwordEncoder.matches("password", check!!.password))
    }

    @Test
    fun successfulRegistrationWithVeryLongAlphabeticPassword() {
        // Create very long password
        val password = RandomStringUtils.randomAlphabetic(1000)
        // Define new user
        val user = mapOf(
                "email" to "long@password", "firstName" to "Long", "lastName" to "Passwd", "password" to password)
        // Create post body for query
        var query = ""
        for((k, v) in user) {
            query += "$k=" + URLDecoder.decode(v, "UTF-8") + "&"
        }
        query = query.dropLast(1)
        mockMvc
                .perform(post("/registration?$query"))
                .andExpect(status().isFound)
                .andExpect(redirectedUrl("/hello"))
        // Check if user got successfully registered
        val check = repository.findByEmailAndFirstNameAndLastName("long@password", "Long", "Passwd")
        assertNotNull(check)
        assert(passwordEncoder.matches(password, check!!.password))
    }

    @Test
    fun successfulRegistrationWithVeryLongAlphanumericPassword() {
        // Create very long password
        val password = RandomStringUtils.randomAlphanumeric(1000)
        // Define new user
        val user = mapOf(
                "email" to "long2@password", "firstName" to "Long", "lastName" to "Passwd", "password" to password)
        // Create post body for query
        var query = ""
        for((k, v) in user) {
            query += "$k=" + URLDecoder.decode(v, "UTF-8") + "&"
        }
        query = query.dropLast(1)
        mockMvc
                .perform(post("/registration?$query"))
                .andExpect(status().isFound)
                .andExpect(redirectedUrl("/hello"))
        // Check if user got successfully registered
        val check = repository.findByEmailAndFirstNameAndLastName("long2@password", "Long", "Passwd")
        assertNotNull(check)
        assert(passwordEncoder.matches(password, check!!.password))
    }

    @Test
    fun successfulRegistrationWithUnicodePassword() {
        // Create very long password
        val password = "\u30AB\u30C4\u30DA\u30EB"
        // Define new user
        val user = mapOf(
                "email" to "unicode@password", "firstName" to "U", "lastName" to "Passwd", "password" to password)
        // Create post body for query
        var query = ""
        for((k, v) in user) {
            query += "$k=" + URLDecoder.decode(v, "UTF-8") + "&"
        }
        query = query.dropLast(1)
        mockMvc
                .perform(post("/registration?$query"))
                .andExpect(status().isFound)
                .andExpect(redirectedUrl("/hello"))
        // Check if user got successfully registered
        val check = repository.findByEmailAndFirstNameAndLastName("unicode@password", "U", "Passwd")
        assertNotNull(check)
        assert(passwordEncoder.matches(password, check!!.password))
    }

    @Test
    fun unsuccessfulRegistrationBecauseOfAlreadyInUseEmail() {
        // Define new valid user
        val validUser = mapOf(
                "email" to "neweruser@test", "firstName" to "Newer", "lastName" to "User", "password" to "password")
        // Create post body for query
        var query = ""
        for((k, v) in validUser) {
            query += "$k=$v&"
        }
        query = query.dropLast(1)
        mockMvc.perform(post("/registration?$query"))
        // Define new invalid user
        val invalidUser = mapOf(
                "email" to "neweruser@test", "firstName" to "Newest", "lastName" to "User", "password" to "password1234")
        // Create post body for query
        query = ""
        for((k, v) in invalidUser) {
            query += "$k=$v&"
        }
        query = query.dropLast(1)
        mockMvc
                .perform(post("/registration?$query"))
                .andExpect(status().isOk)
        // Check if invalid was not registered
        assertNull(repository.findByEmailAndFirstNameAndLastName("neweruser@test", "Newest", "User"))
    }

    @Test
    fun unsuccessfulRegistrationBecauseOfMissingEmail() {
        // Define new invalid user
        val user = mapOf(
                "firstName" to "Invalid", "lastName" to "User", "password" to "passwd")
        // Create post body for query
        var query = ""
        for((k, v) in user) {
            query += "$k=" + URLDecoder.decode(v, "UTF-8") + "&"
        }
        query = query.dropLast(1)
        assertThrows<NestedServletException> {
            mockMvc.perform(post("/registration?$query"))
        }
    }

    @Test
    fun unsuccessfulRegistrationBecauseOfMissingFirstName() {
        // Define new invalid user
        val user = mapOf(
                "email" to "invalid@user", "lastName" to "User", "password" to "passwd")
        // Create post body for query
        var query = ""
        for((k, v) in user) {
            query += "$k=" + URLDecoder.decode(v, "UTF-8") + "&"
        }
        query = query.dropLast(1)
        assertThrows<NestedServletException> {
            mockMvc.perform(post("/registration?$query"))
        }
    }

    @Test
    fun unsuccessfulRegistrationBecauseOfMissingLastName() {
        // Define new invalid user
        val user = mapOf(
                "email" to "invalid@user", "firstName" to "Invalid", "password" to "passwd")
        // Create post body for query
        var query = ""
        for((k, v) in user) {
            query += "$k=" + URLDecoder.decode(v, "UTF-8") + "&"
        }
        query = query.dropLast(1)
        assertThrows<NestedServletException> {
            mockMvc.perform(post("/registration?$query"))
        }
    }

    @Test
    fun unsuccessfulRegistrationBecauseOfMissingPassword() {
        // Define new invalid user
        val user = mapOf(
                "email" to "invalid@user", "firstName" to "Invalid", "lastName" to "User")
        // Create post body for query
        var query = ""
        for((k, v) in user) {
            query += "$k=" + URLDecoder.decode(v, "UTF-8") + "&"
        }
        query = query.dropLast(1)
        assertThrows<NestedServletException> {
            mockMvc.perform(post("/registration?$query"))
        }
    }

    @Test
    fun unsuccessfulRegistrationBecauseOfFalseParameters() {
        // Define new invalid user
        val user = mapOf(
                "some" to "thing", "://" to "?%?", "\\(._.)/" to "Hi")
        // Create post body for query
        var query = ""
        for((k, v) in user) {
            query += "$k=$v&"
        }
        query = query.dropLast(1)
        assertThrows<NestedServletException> {
            mockMvc.perform(post("/registration?$query"))
        }
    }

    @Test
    fun registrationAndLoginAndSecuredPageAccess() {
        // Define new user
        val user = mapOf(
                "email" to "john@doe", "firstName" to "John", "lastName" to "Doe", "password" to "passwd1234")
        // Create post body for query
        var query = ""
        for((k, v) in user) {
            query += "$k=$v&"
        }
        query = query.dropLast(1)
        mockMvc
                .perform(post("/registration?$query"))
                .andExpect(status().isFound)
                .andExpect(redirectedUrl("/hello"))
        // Check if user got successfully registered
        val check = repository.findByEmailAndFirstNameAndLastName("john@doe", "John", "Doe")
        assertNotNull(check)
        assert(passwordEncoder.matches("passwd1234", check!!.password))
        // Login with newly created user to authenticate and to obtain session
        val session = mockMvc
                .perform(formLogin().user("john@doe").password("passwd1234"))
                .andExpect(status().isFound)
                .andExpect(redirectedUrl("/hello"))
                .andExpect(authenticated().withUsername("john@doe"))
                .andReturn().request.session
        // Access secured page with the newly created user
        mockMvc
                .perform(get("/hello").session(session as MockHttpSession))
                .andExpect(status().isOk)
                .andExpect(content().string(containsString("Hello john@doe!")))
    }

    @Test
    fun passwordsAreNotStoredInPlaintext() {
        // Define new user
        val user = mapOf(
                "email" to "db@check", "firstName" to "DB", "lastName" to "Check", "password" to "plaintext")
        // Create post body for query
        var query = ""
        for((k, v) in user) {
            query += "$k=$v&"
        }
        query = query.dropLast(1)
        mockMvc
                .perform(post("/registration?$query"))
                .andExpect(status().isFound)
                .andExpect(redirectedUrl("/hello"))
        // Check if user got successfully registered
        assertNull(repository.findByEmailAndFirstNameAndLastNameAndPassword(
                "db@check", "DB", "Check", "plaintext"))
        val check = repository.findByEmailAndFirstNameAndLastName("db@check", "DB", "Check")
        assertNotNull(check)
        assert(passwordEncoder.matches("plaintext", check!!.password))
    }

}
